'''
Created on Nov 12, 2015

@author: tsorensen
'''
import heapq
from copy import deepcopy
import sokoban_solver
import Queue
#from multiprocessing import Queue as ThreadQueue

floor = 0
wall = 1 
goal = 2 
gem_floor = 3 
gem_goal = 4
player = 5
player_goal = 6



class State(object):
    def __init__(self, cells):
        self.cells = cells
        self.parent = None
        self.f = 0
        self.g = 0
        self.h = 0
        self.history = set()
        
    def getString(self):
        chArr = ""
        for cell in self.cells:
            if cell.type == wall:
                chArr += '#'
            elif cell.type == floor:
                chArr += ' '
            elif cell.type == player:
                chArr += '@'
            elif cell.type == gem_floor:
                chArr += '$'
            elif cell.type == gem_goal:
                chArr += '%'
            elif cell.type == goal:
                chArr += '.'
            elif cell.type == player_goal:
                chArr += '+'
                
        return chArr
        
    def __hash__(self):
        return hash(self.getString())
        
    def __eq__(self, other):
        sameO = True
        
        for x in range(0, len(self.cells)):
            if self.cells[x].type != other.cells[x].type:
                sameO = False
                break
        
        return sameO
        

class Cell(object):
    def __init__(self, x, y, reachable, typeA):
        self.reachable = reachable
        self.x = x
        self.y = y
        self.type = typeA
        
        
class RankLevel(object):
    def __init__(self):
        self.opened = []
        heapq.heapify(self.opened)
        self.cells = []
        self.closed = set()
        self.grid_height = 0
        self.grid_width = 0
        self.winning_set = []
        
    def init_grid(self, levelString):
        self.cells = []
        count = 0
        countLines = 0
        for ch in levelString:
            if (ch == '\n' or ch == '!') and self.grid_width == 0:
                self.grid_width = count
            
            if ch =='\n' or ch =='!':
                countLines = countLines + 1
                
            if ch == '#':
                self.cells.append(Cell(count, countLines, False, wall))  
            elif ch == '.':
                self.cells.append(Cell(count, countLines, True, goal))
            elif ch == '%':
                self.cells.append(Cell(count, countLines, True, gem_goal))
            elif ch == '$':
                self.cells.append(Cell(count, countLines, True, gem_floor))
            elif ch == '@':
                self.cells.append(Cell(count, countLines, True, player))
            elif ch == ' ':
                self.cells.append(Cell(count, countLines, True, floor))
            elif ch == '+':
                self.cells.append(Cell(count, countLines, True, player_goal))
                
                
            count = count+1
                
        self.grid_height = countLines+1   
        self.start = State(deepcopy(self.cells))
        
    def create_winning_set(self, level, solution):
        
        chArray = list(level)
        
        width = 0
        for c in chArray:
            if c == '\n' or c =='!':
                break
            width = width + 1
        width = width+1    
        
        self.winning_set.append(''.join(chArray));
        
        for x in range(0,len(solution)-1):
            ch = solution[x]
        
            start_index = 0
            player_on_goal = False
            for c in chArray:
                if c == '@':
                    break
                elif c == '+':
                    player_on_goal = True
                    break
                start_index = start_index + 1
                
            
            if ch == 'd':
                if player_on_goal:
                    chArray[start_index] = '.'
                else:
                    chArray[start_index] = ' '
                
                if chArray[start_index+width] == '.':
                    chArray[start_index+width] = '+'
                else:
                    chArray[start_index+width] = '@'
            elif ch == 'D':
                if player_on_goal:
                    chArray[start_index] = '.'
                else:
                    chArray[start_index] = ' '
                
                if chArray[start_index+(width*2)] == '.':
                    chArray[start_index+(width*2)] = '%'
                else:
                    chArray[start_index+(width*2)] = '$'
                
                if chArray[start_index+width] == '%':
                    chArray[start_index+width] = '+'
                else:
                    chArray[start_index+width] = '@'
            elif ch == 'u':
                if player_on_goal:
                    chArray[start_index] = '.'
                else:
                    chArray[start_index] = ' '
                
                if chArray[start_index-width] == '.':
                    chArray[start_index-width] = '+'
                else:
                    chArray[start_index-width] = '@'
            elif ch == 'U':
                if player_on_goal:
                    chArray[start_index] = '.'
                else:
                    chArray[start_index] = ' '
                
                if chArray[start_index-(width*2)] == '.':
                    chArray[start_index-(width*2)] = '%'
                else:
                    chArray[start_index-(width*2)] = '$'
                
                if chArray[start_index-width] == '%':
                    chArray[start_index-width] = '+'
                else:
                    chArray[start_index-width] = '@'
            elif ch == 'l':
                if player_on_goal:
                    chArray[start_index] = '.'
                else:
                    chArray[start_index] = ' '
                
                if chArray[start_index-1] == '.':
                    chArray[start_index-1] = '+'
                else:
                    chArray[start_index-1] = '@'
            elif ch == 'L':
                if player_on_goal:
                    chArray[start_index] = '.'
                else:
                    chArray[start_index] = ' '
                
                if chArray[start_index-2] == '.':
                    chArray[start_index-2] = '%'
                else:
                    chArray[start_index-2] = '$'
                
                if chArray[start_index-1] == '%':
                    chArray[start_index-1] = '+'
                else:
                    chArray[start_index-1] = '@'
            elif ch == 'r':
                if player_on_goal:
                    chArray[start_index] = '.'
                else:
                    chArray[start_index] = ' '
                
                if chArray[start_index+1] == '.':
                    chArray[start_index+1] = '+'
                else:
                    chArray[start_index+1] = '@'
            elif ch == 'R':
                if player_on_goal:
                    chArray[start_index] = '.'
                else:
                    chArray[start_index] = ' '
                
                if chArray[start_index+2] == '.':
                    chArray[start_index+2] = '%'
                else:
                    chArray[start_index+2] = '$'
                
                if chArray[start_index+1] == '%':
                    chArray[start_index+1] = '+'
                else:
                    chArray[start_index+1] = '@'
            self.winning_set.append(''.join(chArray));
        
    def is_in_set(self, current):
        if current in self.winning_set:
            return True
        else:
            return False
        
            
    def get_cell(self, x, y):
        return self.cells[x * self.grid_height + y]
    
    def get_adjacent_cells(self, current):
        
        x = 0
        y = 0
        for ch in current:
            if ch.type == player or ch.type == player_goal:
                break
            
            if x >= self.grid_width:
                x = 0
                y = y+1
            
            x = x + 1
          
        allNeighbors = []
        
        if x < self.grid_width-2:
            
            if current[x+1 + self.grid_width * y].type != wall:
                if current[x+1 + self.grid_width * y].type == floor or current[x+1 + self.grid_width * y].type == goal:
                    cells1 = deepcopy(current);
                    
                    if current[x+1 + self.grid_width * y].type == goal:
                        cells1[x+1 + self.grid_width * y].type = player_goal
                    else:
                        cells1[x+1 + self.grid_width * y].type = player
                     
                    if current[x + self.grid_width * y].type == player:
                        cells1[x + self.grid_width * y].type = floor
                    else:
                        cells1[x + self.grid_width * y].type = goal  
                        
                    
                    allNeighbors.append(State(cells1))
                elif current[x+1 + self.grid_width * y].type == gem_floor or current[x+1 + self.grid_width * y].type == gem_goal:
                    if x < self.grid_width-3:
                        if current[x+2 + self.grid_width * y].type != wall and current[x+2 + self.grid_width * y].type != gem_floor and current[x+2 + self.grid_width * y].type != gem_goal:
                            cells1 = deepcopy(current);
                            if current[x+2 + self.grid_width * y].type == goal:
                                cells1[x+2 + self.grid_width * y].type = gem_goal
                            else:  
                                cells1[x+2 + self.grid_width * y].type = gem_floor
                                
                            if current[x+1 + self.grid_width * y].type == gem_goal:
                                cells1[x+1 + self.grid_width * y].type = player_goal
                            else:
                                cells1[x+1 + self.grid_width * y].type = player
                                
                            if current[x + self.grid_width * y].type == player:
                                cells1[x + self.grid_width * y].type = floor
                            else:
                                cells1[x + self.grid_width * y].type = goal
                                
                            allNeighbors.append(State(cells1)) 
                    
        if y > 0: 

            if current[x + self.grid_width * (y-1)].type != wall:
                if current[x + self.grid_width * (y-1)].type == floor or current[x + self.grid_width * (y-1)].type == goal :
                    cells2 = deepcopy(current);
                    
                    if current[x + self.grid_width * (y-1)].type == goal:
                        cells2[x + self.grid_width * (y-1)].type = player_goal
                    else:
                        cells2[x + self.grid_width * (y-1)].type = player
                        
                    if current[x + self.grid_width * y].type == player: 
                        cells2[x + self.grid_width * y].type = floor
                    else:
                        cells2[x + self.grid_width * y].type = goal
    
            
                    allNeighbors.append(State(cells2))
                elif current[x + self.grid_width * (y-1)].type == gem_floor or current[x + self.grid_width * (y-1)].type == gem_goal:
                    if y > 1:
                        if current[x + self.grid_width * (y-2)].type != wall and current[x + self.grid_width * (y-2)].type != gem_floor and current[x + self.grid_width * (y-2)].type != gem_goal:
                            cells2 = deepcopy(current);
                            
                            if current[x + self.grid_width * (y-2)].type == goal:
                                cells2[x + self.grid_width * (y-2)].type = gem_goal
                            else:  
                                cells2[x + self.grid_width * (y-2)].type = gem_floor
                            
                            if current[x + self.grid_width * (y-1)].type == gem_floor:     
                                cells2[x + self.grid_width * (y-1)].type = player
                            else:
                                cells2[x + self.grid_width * (y-1)].type = player_goal
                             
                            if current[x + self.grid_width * y].type == player:       
                                cells2[x + self.grid_width * y].type = floor
                            else:
                                cells2[x + self.grid_width * y].type = goal
                                
                            allNeighbors.append(State(cells2)) 
                        
        if x > 0:
          
            if current[x-1 + self.grid_width * y].type != wall:
                if current[x-1 + self.grid_width * y].type == floor or  current[x-1 + self.grid_width * y].type == goal:
                    cells3 = deepcopy(current);
                    
                    if current[x-1 + self.grid_width * y].type == goal:
                        cells3[x-1 + self.grid_width * y].type = player_goal
                    else:
                        cells3[x-1 + self.grid_width * y].type = player
                     
                    if current[x + self.grid_width * y].type == player:    
                        cells3[x + self.grid_width * y].type = floor
                    else:
                        cells3[x + self.grid_width * y].type = goal
                        
                    allNeighbors.append(State(cells3))
                elif current[x-1 + self.grid_width * y].type == gem_floor or current[x-1 + self.grid_width * y].type == gem_goal:
                    if x > 1:
                        if current[x-2 + self.grid_width * y].type != wall and current[x-2 + self.grid_width * y].type != gem_floor and current[x-2 + self.grid_width * y].type != gem_goal:
                            cells3 = deepcopy(current);
                            if current[x-2 + self.grid_width * y].type == goal:
                                cells3[x-2 + self.grid_width * y].type = gem_goal
                            else:  
                                cells3[x-2 + self.grid_width * y].type = gem_floor
                                
                            if current[x-1 + self.grid_width * y].type == gem_floor:   
                                cells3[x-1 + self.grid_width * y].type = player
                            else:
                                cells3[x-1 + self.grid_width * y].type = player_goal
                            
                            if current[x + self.grid_width * y].type == player:    
                                cells3[x + self.grid_width * y].type = floor
                            else:
                                cells3[x + self.grid_width * y].type = goal
                                
                            allNeighbors.append(State(cells3))  
  
        if y < self.grid_height-2:
            
            if current[x + self.grid_width * (y+1)].type != wall:
                if current[x + self.grid_width * (y+1)].type == floor or current[x + self.grid_width * (y+1)].type == goal:
                    cells4 = deepcopy(current);
                    
                    if current[x + self.grid_width * (y+1)].type == goal:
                        cells4[x + self.grid_width * (y+1)].type = player_goal
                    else:
                        cells4[x + self.grid_width * (y+1)].type = player
                     
                    if current[x + self.grid_width * y].type == player:   
                        cells4[x + self.grid_width * y].type = floor
                    else:
                        cells4[x + self.grid_width * y].type = goal 
                        
                    allNeighbors.append(State(cells4))
                elif current[x + self.grid_width * (y+1)].type == gem_floor or current[x + self.grid_width * (y+1)].type == gem_goal:
                    if y < self.grid_width-3:
                        if current[x + self.grid_width * (y+2)].type != wall and current[x + self.grid_width * (y+2)].type != gem_floor and current[x + self.grid_width * (y+2)].type != gem_goal:
                            cells4 = deepcopy(current);
                            if current[x + self.grid_width * (y+2)].type == goal:
                                cells4[x + self.grid_width * (y+2)].type = gem_goal
                            else:  
                                cells4[x + self.grid_width * (y+2)].type = gem_floor
                                
                            if current[x + self.grid_width * (y+1)].type == gem_floor:    
                                cells4[x + self.grid_width * (y+1)].type = player
                            else:
                                cells4[x + self.grid_width * (y+1)].type = player_goal
                                
                            if current[x + self.grid_width * y].type == player:
                                cells4[x + self.grid_width * y].type = floor
                            else:
                                cells4[x + self.grid_width * y].type = goal
                                
                            allNeighbors.append(State(cells4)) 
                        
        
        return allNeighbors
    
    def getString(self, myMap):
        chArr = ""
        row = 0
        for cell in myMap:
            if cell.type == wall:
                chArr += '#'
            elif cell.type == floor:
                chArr += ' '
            elif cell.type == player:
                chArr += '@'
            elif cell.type == gem_floor:
                chArr += '$'
            elif cell.type == gem_goal:
                chArr += '%'
            elif cell.type == goal:
                chArr += '.'
            elif cell.type == player_goal:
                chArr += '+'
                
            row = row + 1
            if row >= self.grid_width:
                chArr += '\n'
                row = 0
        
        return chArr
    
    def print_map(self, myMap):    
        print self.getString(myMap)
    
    
    def number_of_badMoves(self):
        dead_ends = 0
        for move in self.winning_set:

            self.init_grid(move)
            states = set()
            stripped_states = set()
            que = Queue.Queue()
            que.put(self.start)
            #self.print_map(self.start.cells)
            
            for depth in range(0,10):
                if que.empty():
                    break
                
                temp = que.get()
                adj_states = self.get_adjacent_cells(temp.cells)
                for adj_state in adj_states:
                    strippe_me = deepcopy(adj_state)
                    
                    for cell in strippe_me.cells:
                        if cell.type == player:
                            cell.type = floor
                        if cell.type == player_goal:
                            cell.type = goal
                            
                    
                    if (adj_state not in states) and (strippe_me not in stripped_states):
                        que.put(adj_state)
                        states.add(adj_state)
                        stripped_states.add(strippe_me)
                
            
            for state in states:
                #self.print_map(state.cells)
                sokoban_solver.init(self.getString(state.cells))
                outgoing = Queue.Queue()
                catch = sokoban_solver.solve(5,outgoing)
                
                
                if catch == "No Solution":
                    #self.print_map(state.cells)
                    dead_ends = dead_ends+1
                
                #reset
                sokoban_solver.data = []
                sokoban_solver.nrows = 0
                sokoban_solver.px = sokoban_solver.py = 0
                sokoban_solver.sdata = ""
                sokoban_solver.ddata = ""
            
        return dead_ends
    
    


#level="""\
##########
#   ###  #
# #   #  #
#@  # #  #
## #  . ##
## # #   #
## # $   #
#  ### # #
# $ ## # #
##. ## # #
####     #
####     #
##########
#"""
#solution = "rddddDuuuuururrddlddRRurrdLrddddlluuuUU"


#rank = RankLevel()
#rank.create_winning_set(level, solution)
#rank.init_grid(level)
#print rank.number_of_badMoves()
