'''
Created on Oct 31, 2015

@author: Tyson
'''
import mysql.connector
from mysql.connector import errorcode



def parseFile(which):
    myFile = open("levelSet01.txt", 'r')
    myFile.seek(-79, 2)
    maxMoves = int(myFile.readline())
    print maxMoves
    myFile.seek(-65, 2)
    minMoves = int(myFile.readline())
    print minMoves
    myFile.seek(-50, 2)
    maxPush = int(myFile.readline())
    print maxPush
    myFile.seek(-35, 2)
    minPush = int(myFile.readline())
    print minPush
    myFile.seek(-19, 2)
    maxBoxLines = int(myFile.readline())
    print maxBoxLines
    myFile.seek(-3, 2)
    minBoxLines = int(myFile.readline())
    print minBoxLines
    myFile.seek(0,0)
    level = ""
    solution = ""
    rank = ""
    
    
    found = False
    while not found:
        level = ""
        temp = myFile.readline()
        temp = myFile.readline()
        while(temp[0] == '#'):
            level = level + temp
            temp = myFile.readline()
        
        solution = temp
        temp = myFile.readline().split(" ")
        moves = temp[1]
        temp = myFile.readline().split(" ")
        push = temp[1]
        temp = myFile.readline().split(" ")
        lines = temp[1]
        temp = myFile.readline()
        
        if int(moves) == maxMoves and which == 0:
            found = True
        if int(push) == maxPush and which == 1:
            found = True
        if int(lines) == maxBoxLines and which == 2:
            found = True
    
    tL = list(level)
    for x in range(0, len(tL)):
        if tL[x] == '\n':
            tL[x] = '!'

    level = ''.join(tL)
    #print level
    return (level, solution, moves, push, lines, 0, 0)
    

def mysqlInsertLevel():
    try:
        cnx = mysql.connector.connect(user='root', password='root',
                                  host='localhost',
                                  database='sokobanlevel',
                                  port='8889')
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    
    print "good to go"
    
    cursor = cnx.cursor()
    add_level = ("INSERT INTO `Levels`"
                   "(`level_Str`, `level_Solution`, `moves_Rank`, `push_Rank`, `boxLine_Rank`, `user_Rank`, `my_Rank`)"
                   "VALUES (%s, %s, %s, %s, %s, %s, %s)")

    add_count = ("UPDATE`Info` SET `games`=`games`+1 WHERE 1")
    
    
    #data_level = (strLevel, " ", "1", "1", "1", "1", "1")
    data_level = parseFile(0)
    
    cursor.execute(add_level, data_level)
    cnx.commit()
    
    cursor.execute(add_count)
    cnx.commit()
    
    data_level2 = parseFile(1)
    
    if not (data_level == data_level2):
        cursor.execute(add_level, data_level2)
        cnx.commit()
    
        cursor.execute(add_count)
        cnx.commit()
    
    data_level3 = parseFile(2)
    
    if not (data_level == data_level2) and not(data_level == data_level3) and not(data_level2 == data_level3):
        cursor.execute(add_level, data_level3)
        cnx.commit()
        
        cursor.execute(add_count)
        cnx.commit()
    
    cursor.close()
    cnx.close()
    
mysqlInsertLevel()
#parseFile(0)
