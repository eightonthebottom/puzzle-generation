'''
Created on Nov 4, 2015

@author: tsorensen
'''
import heapq
import Queue
from copy import deepcopy

floor = 0
wall = 1 
goal = 2 
gem_floor = 3 
gem_goal = 4
player = 5
player_goal = 6

class State(object):
    def __init__(self, cells):
        self.cells = cells
        self.parent = None
        self.f = 0
        self.g = 0
        self.h = 0
        self.history = set()
        
    def getString(self):
        chArr = ""
        for cell in self.cells:
            if cell.type == wall:
                chArr += '#'
            elif cell.type == floor:
                chArr += ' '
            elif cell.type == player:
                chArr += '@'
            elif cell.type == gem_floor:
                chArr += '$'
            elif cell.type == gem_goal:
                chArr += '%'
            elif cell.type == goal:
                chArr += '.'
            elif cell.type == player_goal:
                chArr += '+'
                
        return chArr
        
    def __hash__(self):
        return hash(self.getString())
        
    def __eq__(self, other):
        sameO = True
        
        for x in range(0, len(self.cells)):
            if self.cells[x].type != other.cells[x].type:
                sameO = False
                break
        
        return sameO
        

class Cell(object):
    def __init__(self, x, y, reachable, typeA):
        self.reachable = reachable
        self.x = x
        self.y = y
        self.type = typeA
        
        
class AStar(object):
    def __init__(self):
        self.opened = []
        heapq.heapify(self.opened)
        self.cells = []
        self.closed = set()
        self.grid_height = 0
        self.grid_width = 0
        
    def init_grid(self, levelString):
        
        count = 0
        countLines = 0
        for ch in levelString:
            if ch == '\n' and self.grid_width == 0:
                self.grid_width = count
            
            if ch =='\n':
                countLines = countLines + 1
                
            if ch == '#':
                self.cells.append(Cell(count, countLines, False, wall))  
            elif ch == '.':
                self.cells.append(Cell(count, countLines, True, goal))
            elif ch == '%':
                self.cells.append(Cell(count, countLines, True, gem_goal))
            elif ch == '$':
                self.cells.append(Cell(count, countLines, True, gem_floor))
            elif ch == '@':
                self.cells.append(Cell(count, countLines, True, player))
            elif ch == ' ':
                self.cells.append(Cell(count, countLines, True, floor))
            elif ch == '+':
                self.cells.append(Cell(count, countLines, True, player_goal))
                
                
            count = count+1
                
        self.grid_height = countLines+1   
        self.start = State(deepcopy(self.cells))
        
    def is_solved(self, current):
        for cell in current:
            if cell.type == gem_floor:
                return False
        return True
            
        
    def get_heuristic(self, mapA):
        #find goals x y and find gems x y
        goals = []
        gems = []
        playPos = (0,0)
        for cell in mapA.cells:
            if cell.type == goal or cell.type == player_goal:
                goals.append((cell.x,cell.y))
                if cell.type == player_goal:
                    playPos = (cell.x, cell.y)
            if cell.type == gem_floor:
                gems.append((cell.x,cell.y))
            if cell.type == player: 
                playPos = (cell.x, cell.y)  
            
        
        totalMoves = 0
                
        #count moves to completions
        for g in goals:
            #find closes goal gem pair
            min_Index = 0
            min_distance = 10000000
            i = 0
            for gem in gems:
                tempX = abs(gem[0] - g[0])
                tempY = abs(gem[1] - g[1])
                
                if tempX+tempY < min_distance:
                    min_distance = tempX+tempY
                    min_Index = i
                    
                i = i + 1
            
            gems.remove(gems[min_Index])
            totalMoves += min_distance
            
        #count moves from player to each goal
        for g in goals:
            tempX = abs(g[0]-playPos[0])
            tempY = abs(g[1]-playPos[1])
            totalMoves = totalMoves + (tempX+tempY)
               
        return totalMoves
    
    def get_cell(self, x, y):
        return self.cells[x * self.grid_height + y]
    
    def get_adjacent_cells(self, current):
        
        x = 0
        y = 0
        for ch in current:
            if ch.type == player or ch.type == player_goal:
                break
            
            if x >= self.grid_width:
                x = 0
                y = y+1
            
            x = x + 1
          
        allNeighbors = []
        
        if x < self.grid_width-2:
            
            if current[x+1 + self.grid_width * y].type != wall:
                if current[x+1 + self.grid_width * y].type == floor or current[x+1 + self.grid_width * y].type == goal:
                    cells1 = deepcopy(current);
                    
                    if current[x+1 + self.grid_width * y].type == goal:
                        cells1[x+1 + self.grid_width * y].type = player_goal
                    else:
                        cells1[x+1 + self.grid_width * y].type = player
                     
                    if current[x + self.grid_width * y].type == player:
                        cells1[x + self.grid_width * y].type = floor
                    else:
                        cells1[x + self.grid_width * y].type = goal  
                        
                    
                    allNeighbors.append(State(cells1))
                elif current[x+1 + self.grid_width * y].type == gem_floor or current[x+1 + self.grid_width * y].type == gem_goal:
                    if x < self.grid_width-3:
                        if current[x+2 + self.grid_width * y].type != wall and current[x+2 + self.grid_width * y].type != gem_floor and current[x+2 + self.grid_width * y].type != gem_goal:
                            cells1 = deepcopy(current);
                            if current[x+2 + self.grid_width * y].type == goal:
                                cells1[x+2 + self.grid_width * y].type = gem_goal
                            else:  
                                cells1[x+2 + self.grid_width * y].type = gem_floor
                                
                            if current[x+1 + self.grid_width * y].type == gem_goal:
                                cells1[x+1 + self.grid_width * y].type = player_goal
                            else:
                                cells1[x+1 + self.grid_width * y].type = player
                                
                            if current[x + self.grid_width * y].type == player:
                                cells1[x + self.grid_width * y].type = floor
                            else:
                                cells1[x + self.grid_width * y].type = goal
                                
                            allNeighbors.append(State(cells1)) 
                    
        if y > 0: 

            if current[x + self.grid_width * (y-1)].type != wall:
                if current[x + self.grid_width * (y-1)].type == floor or current[x + self.grid_width * (y-1)].type == goal :
                    cells2 = deepcopy(current);
                    
                    if current[x + self.grid_width * (y-1)].type == goal:
                        cells2[x + self.grid_width * (y-1)].type = player_goal
                    else:
                        cells2[x + self.grid_width * (y-1)].type = player
                        
                    if current[x + self.grid_width * y].type == player: 
                        cells2[x + self.grid_width * y].type = floor
                    else:
                        cells2[x + self.grid_width * y].type = goal
    
            
                    allNeighbors.append(State(cells2))
                elif current[x + self.grid_width * (y-1)].type == gem_floor or current[x + self.grid_width * (y-1)].type == gem_goal:
                    if y > 1:
                        if current[x + self.grid_width * (y-2)].type != wall and current[x + self.grid_width * (y-2)].type != gem_floor and current[x + self.grid_width * (y-2)].type != gem_goal:
                            cells2 = deepcopy(current);
                            
                            if current[x + self.grid_width * (y-2)].type == goal:
                                cells2[x + self.grid_width * (y-2)].type = gem_goal
                            else:  
                                cells2[x + self.grid_width * (y-2)].type = gem_floor
                            
                            if current[x + self.grid_width * (y-1)].type == gem_floor:     
                                cells2[x + self.grid_width * (y-1)].type = player
                            else:
                                cells2[x + self.grid_width * (y-1)].type = player_goal
                             
                            if current[x + self.grid_width * y].type == player:       
                                cells2[x + self.grid_width * y].type = floor
                            else:
                                cells2[x + self.grid_width * y].type = goal
                                
                            allNeighbors.append(State(cells2)) 
                        
        if x > 0:
          
            if current[x-1 + self.grid_width * y].type != wall:
                if current[x-1 + self.grid_width * y].type == floor or  current[x-1 + self.grid_width * y].type == goal:
                    cells3 = deepcopy(current);
                    
                    if current[x-1 + self.grid_width * y].type == goal:
                        cells3[x-1 + self.grid_width * y].type = player_goal
                    else:
                        cells3[x-1 + self.grid_width * y].type = player
                     
                    if current[x + self.grid_width * y].type == player:    
                        cells3[x + self.grid_width * y].type = floor
                    else:
                        cells3[x + self.grid_width * y].type = goal
                        
                    allNeighbors.append(State(cells3))
                elif current[x-1 + self.grid_width * y].type == gem_floor or current[x-1 + self.grid_width * y].type == gem_goal:
                    if x > 1:
                        if current[x-2 + self.grid_width * y].type != wall and current[x-2 + self.grid_width * y].type != gem_floor and current[x-2 + self.grid_width * y].type != gem_goal:
                            cells3 = deepcopy(current);
                            if current[x-2 + self.grid_width * y].type == goal:
                                cells3[x-2 + self.grid_width * y].type = gem_goal
                            else:  
                                cells3[x-2 + self.grid_width * y].type = gem_floor
                                
                            if current[x-1 + self.grid_width * y].type == gem_floor:   
                                cells3[x-1 + self.grid_width * y].type = player
                            else:
                                cells3[x-1 + self.grid_width * y].type = player_goal
                            
                            if current[x + self.grid_width * y].type == player:    
                                cells3[x + self.grid_width * y].type = floor
                            else:
                                cells3[x + self.grid_width * y].type = goal
                                
                            allNeighbors.append(State(cells3))  
  
        if y < self.grid_height-2:
            
            if current[x + self.grid_width * (y+1)].type != wall:
                if current[x + self.grid_width * (y+1)].type == floor or current[x + self.grid_width * (y+1)].type == goal:
                    cells4 = deepcopy(current);
                    
                    if current[x + self.grid_width * (y+1)].type == goal:
                        cells4[x + self.grid_width * (y+1)].type = player_goal
                    else:
                        cells4[x + self.grid_width * (y+1)].type = player
                     
                    if current[x + self.grid_width * y].type == player:   
                        cells4[x + self.grid_width * y].type = floor
                    else:
                        cells4[x + self.grid_width * y].type = goal 
                        
                    allNeighbors.append(State(cells4))
                elif current[x + self.grid_width * (y+1)].type == gem_floor or current[x + self.grid_width * (y+1)].type == gem_goal:
                    if y < self.grid_width-3:
                        if current[x + self.grid_width * (y+2)].type != wall and current[x + self.grid_width * (y+2)].type != gem_floor and current[x + self.grid_width * (y+2)].type != gem_goal:
                            cells4 = deepcopy(current);
                            if current[x + self.grid_width * (y+2)].type == goal:
                                cells4[x + self.grid_width * (y+2)].type = gem_goal
                            else:  
                                cells4[x + self.grid_width * (y+2)].type = gem_floor
                                
                            if current[x + self.grid_width * (y+1)].type == gem_floor:    
                                cells4[x + self.grid_width * (y+1)].type = player
                            else:
                                cells4[x + self.grid_width * (y+1)].type = player_goal
                                
                            if current[x + self.grid_width * y].type == player:
                                cells4[x + self.grid_width * y].type = floor
                            else:
                                cells4[x + self.grid_width * y].type = goal
                                
                            allNeighbors.append(State(cells4)) 
                        
        
        return allNeighbors
    
    def print_map(self, myMap):
        chArr = ""
        row = 0
        for cell in myMap:
            if cell.type == wall:
                chArr += '#'
            elif cell.type == floor:
                chArr += ' '
            elif cell.type == player:
                chArr += '@'
            elif cell.type == gem_floor:
                chArr += '$'
            elif cell.type == gem_goal:
                chArr += '%'
            elif cell.type == goal:
                chArr += '.'
            elif cell.type == player_goal:
                chArr += '+'
                
            row = row + 1
            if row >= self.grid_width:
                chArr += '\n'
                row = 0
                
        print chArr
            
            
    def display_path(self, mapA):
        #self.print_map(mapA);
        cell = mapA
        while cell.parent is not self.start:
            cell = cell.parent
            self.print_map(cell.cells)
            
    
    def update_cell(self, adj, orig):
        adj.g = orig.g + 10
        adj.h = self.get_heuristic(adj)
        adj.parent = orig
        adj.f = adj.h + adj.g
    
        
    def process(self):
        count = 0
        heapq.heappush(self.opened, (0, self.start))
        while len(self.opened):
            f, temp = heapq.heappop(self.opened)
            self.closed.add(temp)
            if self.is_solved(temp.cells):
                print "winner"
                count = count + 1
                self.display_path(temp)
            else:
                adj_states = self.get_adjacent_cells(temp.cells)
                for adj_state in adj_states:
                    if adj_state not in self.closed:
                        #self.print_map(adj_state.cells)
                        if (adj_state.f, adj_state) in self.opened:
                            if adj_state.g > temp.g + 10:
                                self.update_cell(adj_state, temp)
                        else:
                            self.update_cell(adj_state, temp)
                            #self.print_map(adj_state.cells)
                            heapq.heappush(self.opened, (adj_state.f, adj_state))
                        
        print count
        
    def dfs(self):
        stack = []
        visited = set()
        stack.append(self.start)
        while len(stack):
            temp = stack.pop()
            
            if self.is_solved(temp.cells):
                print "winner"
                return True
            
            if temp not in visited:
                visited.add(temp)
                self.print_map(temp.cells)
                adj_states = self.get_adjacent_cells(temp.cells)
                for adj_state in adj_states:
                    stack.append(adj_state)
                    
        print "looser"
        return False
    
    def bfs(self):
        que = Queue.Queue()
        que.put(self.start)
        history = set()
        
        while(not que.empty()):
            temp = que._get()
            
            #self.print_map(temp.cells)
            if self.is_solved(temp.cells):
                print "winner"
                return True
            
            adj_states = self.get_adjacent_cells(temp.cells)
            
            for adj_state in adj_states:
                if adj_state not in history:
                    history.add(adj_state)
                    que.put(adj_state)
            
            print len(history)        
            if len(history) > 100000:
                history.clear()
            