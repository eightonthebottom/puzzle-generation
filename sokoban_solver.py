######################################################
#    Breadth First Search to Solve Sokoban Puzzles   #
#                                                    #
#   returns a string of the shortest move solutions  #
#   or return 'No Solution' if it cannot be solved   #
######################################################

# Original source was taken from: http://rosettacode.org/wiki/Sokoban#Python
# This doesn't match as the solver was not complete and could 
# only handle small puzzles


from array import array
from collections import deque
import Queue
 
# globals 
data = []
nrows = 0
px = py = 0
sdata = ""
ddata = ""
 
# Parses the level string and creates the maps needed to handle map changes
def init(board):
    global data, nrows, sdata, ddata, px, py
    data = filter(None, board.splitlines())
    nrows = max(len(r) for r in data)
 
    maps = {' ':' ', '.': '.', '@':' ', '#':'#', '$':' ', '%':'.', '+':'.'}
    mapd = {' ':' ', '.': ' ', '@':'@', '#':' ', '$':'*', '%':'*', '+':'@'}
 
    for r, row in enumerate(data):
        for c, ch in enumerate(row):
            sdata += maps[ch]
            ddata += mapd[ch]
            if ch == '@' or ch == '+':
                px = c
                py = r
                


# Handles pushing a object, changes map
# Blocks if not possible to move (ie wall on other side ect)
def push(x, y, dx, dy, data):
    if sdata[(y+2*dy) * nrows + x+2*dx] == '#' or \
       data[(y+2*dy) * nrows + x+2*dx] != ' ':
        return None
 
    data2 = array("c", data)
    data2[y * nrows + x] = ' '
    data2[(y+dy) * nrows + x+dx] = '@'
    data2[(y+2*dy) * nrows + x+2*dx] = '*'
    return data2.tostring()
 
# Does a quick check to see if all objects are on goals
def is_solved(data):
    for i in xrange(len(data)):
        if (sdata[i] == '.') != (data[i] == '*'):
            return False
    return True
 
# BFS of level
def solve(throwaway,myQueue):
    open = deque([(ddata, "", px, py)]) #the queue for bfs
    visited = set([ddata])              #keeps track of visited states
    visitOrder = deque([ddata])         #if visited gets too large this will help remove oldest states
    dirs = ((0, -1, 'u', 'U'), ( 1, 0, 'r', 'R'),  #used to build solution string
            (0,  1, 'd', 'D'), (-1, 0, 'l', 'L'))
 
    lnrows = nrows
    while open:
        # get next node to process
        cur, csol, x, y = open.popleft()
 
        # for each direction you can move from selected node
        for di in dirs:
            temp = cur
            dx, dy = di[0], di[1]

            #if node you would be moving too is box
            if temp[(y+dy) * lnrows + x+dx] == '*': 
                # can you push it?
                temp = push(x, y, dx, dy, temp)
                # if you can push it and pushing it doesn't duplicate a state
                if temp and temp not in visited:
                    # check it see if you win
                    if is_solved(temp):
                        tsrt = csol + di[3]
                        myQueue.put(tsrt)  #this is return queue, used so solver runs in different thread with timeout
                        return csol + di[3] #dummy return to end search or to return solution if not in new thread
                    
                    #add this state to queue, visited and visitOrder
                    open.append((temp, csol + di[3], x+dx, y+dy))
                    visited.add(temp)
                    visitOrder.append(temp)
                    
                    #ram manager, make sure visited queue doesn't grow impossible big
                    if len(visited) > 100000:
                        visited.remove(visitOrder.popleft())
           
            else:
                # if node you would be moving too is a wall or not a floor do nothing
                if sdata[(y+dy) * lnrows + x+dx] == '#' or \
                   temp[(y+dy) * lnrows + x+dx] != ' ':
                    continue
 
                #create new state
                data2 = array("c", temp)
                data2[y * lnrows + x] = ' '
                data2[(y+dy) * lnrows + x+dx] = '@'
                temp = data2.tostring()
 
                # if new state is not in visited
                if temp not in visited:
                    # check to see if game is won
                    if is_solved(temp):
                        tempStr = csol + di[2]
                        myQueue.put(tempStr) #this is return queue, used so solver runs in different thread with timeout
                        return csol + di[2] #dummy return to end search or to return solution if not in new thread
                    
                    #add to queues
                    open.append((temp, csol + di[2], x+dx, y+dy))
                    visited.add(temp)
                    visitOrder.append(temp)
                    
                    # manage memory if history gets too big
                    if len(visited) > 100000:
                        visited.remove(visitOrder.popleft())
 
    myQueue.put("No Solution")
    return "No Solution"