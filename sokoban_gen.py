###########################################################
#             Sokoban Puzzle Generation                   #
#                                                         #
#   creates a map and then populates possible puzzles     #
# once it has finished, fill a file with levels and ranks #
# appends to bottom of file the min and max of each rank  # 
###########################################################

#all puzzles added to file are solvable according to sokoban_solver.py


# includes -------------------------------------------------------
import numpy
import random
import sokoban_template
import sys
import Queue
import sokoban_solver
from multiprocessing.pool import Process
from multiprocessing import Queue as ThreadQueue
# end includes ----------------------------------------------------


# Map generator ---------------------------------------------------

# Once a valid template is selected, copy it into map
# @args:   level: current map
#          piece: template piece to be added to map
#          startX: the upper left corner of map to insert template
#          startY: the upper left corner of map to insert template 
#          size: 3 or 5 depending on which template is used
# @return: the new map after template is inserted
def copy(level, piece, startX, startY, size):
	for x in range(0,size):
		for y in range(0,size):
			if startX+x < level.shape[0] and startY+y < level.shape[1]:
				level[startX+x][startY+y] = piece[x+1][y+1]
	return level
#----------------------------------------------------------------------

# Check randomly selected and flipped template can be added to map
# according to the overlap rule
# @args:   level: current map
#          piece: template to be added to map
#          startX: the upper left corner of map to insert template
#          startY: the upper left corner of map to insert template 
#          height: map dimension
#          width:  map dimension
#          size:   5 or 7 depending on template used
# @return: True or False depending if template can be added to map        
def canCopy(level, piece, startX, startY, height, width, size):
	copy = True

	#check top row
	for x in range(0,size):
		if startY+x < width:
			if level[startX][startY+x] != 0:
				if level[startX][startY+x] != piece[0][x]:
					copy = False

	#check left col
	for x in range(0,size):
		if startX+x < height:
			if level[startX+x][startY] != 0:
				if level[startX+x][startY] != piece[x][0]:
					copy = False

	#check right col
	for x in range(0,size):
		if startX+4 < height:
			if startY+x < width:
				if level[startX+4][startY+x] != 0:
					if level[startX+4][startY+x] != piece[4][x]:
						copy = False

	#check bottom row
	for x in range(0,size):
		if startY+4 < width:
			if startX+x < height:
				if level[startX+x][startY+4] != 0:
					if level[startX+x][startY+4] != piece[x][4]:
						copy = False

	return copy
#----------------------------------------------------------------------


# Main Function that creates a valid map
# @args:   minG: min grid height and width
#          maxG: max grid height and width
# @return: the empty map to be used
#          there is a timeout to prevent this loop running forever
#          whatever the current map is when timeout happens
#          timeout is a error count not done by time
def emptyRoom(minG, maxG):
	gridX = 0
	gridY = 0

	random.seed()
	# get random board size
	height = random.randint(minG, maxG)
	width = random.randint(minG, maxG)

	# fill map with 0
	level = numpy.ndarray((height,width))
	level.fill(0)
	

	count = 0
	while True:
		# get a random template
		index = random.randint(0,16)
		
		piece = sokoban_template.template[index]

		# rotate template random time
		rotateTimes = random.randint(0,3)
		piece = numpy.rot90(piece, rotateTimes)


		flip = random.randint(0,3)
		if flip == 1:
			# flip horizontal random
			piece = numpy.fliplr(piece)
		elif flip == 2:
			#flip vertical random
			piece = numpy.flipud(piece)

		tSize = 5
			
		if canCopy(level, piece, gridX, gridY, height, width, tSize):
			level = copy(level, piece, gridX, gridY, (tSize-2))
			gridX = gridX + 3
				
			if gridX > height-1:
				gridY = gridY + 3
				gridX = 0

		#print level.astype(int)

		if gridY > width-1:
			return level

		count = count + 1
		if count > 100000:
			return level
#----------------------------------------------------------------------

# Maker sure that all floors are connected (multirooms are not allowed)
# @args:  level: current map
# 
# @return:  True or False if room is a valid room
def onlyOneContFloor(level):
	good = True
	dp = numpy.ndarray((level.shape[0],level.shape[1]))
	dp.fill(0)

	count = 1

	queue = Queue.Queue()

	#dp wit bfs search to mark continues floors
	for x in range(0,level.shape[0]):
		for y in range(0, level.shape[1]):
			if level[x][y].astype(int) == 1:
				if dp[x][y] == 0:
					queue.put((x,y))
					dp[x][y] = count
					while not queue.empty():
						i,j = queue.get()
						if i+1 < level.shape[0]:
							if level[i+1][j].astype(int) == 1:
								if dp[i+1][j] == 0:
									queue.put((i+1,j))
									dp[i+1][j] = count
						if j+1 < level.shape[1]:
							if level[i][j+1].astype(int) == 1:
								if dp[i][j+1] == 0:
									queue.put((i,j+1))
									dp[i][j+1] = count
						if j-1 >= 0:
							if level[i][j-1].astype(int) == 1:
								if dp[i][j-1] == 0:
									queue.put((i,j-1))
									dp[i][j-1] = count
						if i-1 >= 0:
							if level[i-1][j].astype(int) == 1:
								if dp[i-1][j] == 0:
									queue.put((i-1,j))
									dp[i-1][j] = count

					count = count + 1

	#print level.astype(int)
	#print dp.astype(int)

	#check dp to see if map is valid
	for row in dp:
		for i in row:
			if i > 1:
				good = False

	return good
#----------------------------------------------------------------------

# Checks the map to see if there is a large empty space, these are thrown
# out because the just waste memory and don't add to level play
#
# @args: level: current map
#
# @return: True or False if room is valid
def noOpenFloorToBig(level):

	if level.shape[0] < 3 or level.shape[1] < 3:
		return True
	
	# for each floor pretend it is top corner and check right and down
	# since you start at top left this will catch all floor spaces
	for x in range(0, level.shape[0]-2):
		for y in range(0, level.shape[1]-2):
			tooBig = True
			if level[x][y].astype(int) == 1:
				for i in range(0,2):
					for j in range(0,2):
						if(not(level[x+i][y+j].astype(int) == 1)):
							tooBig = False

				if tooBig:
					if x+3 < level.shape[0] and y+2 < level.shape[1]:
						if level[x+3][y] == 1 and level[x+3][y+1] == 1 and level[x+3][y+2] == 1:
							return False
					if y+3 < level.shape[1] and x+2 < level.shape[0]:
						if level[x][y+3] == 1 and level[x+1][y+3] == 1 and level[x+2][y+3] == 1:
							return False
					
	return True
#----------------------------------------------------------------------

# Checks map to make sure there aren't any trapped floor spaces
# trapped floors have walls on three sides
# Trapped floors are too easy to deal with
#
# @args: level: current map
#
# @return: True or False if map is valid
def noTrappedFloors(level):

	for x in range(0,level.shape[0]):
		for y in range(0, level.shape[1]):
			if level[x][y].astype(int) == 1:
				wallCount = 0
				if x+1 < level.shape[0]:
					if level[x+1][y].astype(int) == 2:
						wallCount = wallCount + 1
				else:
					wallCount = wallCount + 1

				if x-1 >= 0:
					if level[x-1][y].astype(int) == 2:
						wallCount = wallCount + 1
				else:
					wallCount = wallCount + 1

				if y+1 < level.shape[1]:
					if level[x][y+1].astype(int) == 2:
						wallCount = wallCount + 1
				else:
					wallCount = wallCount + 1

				if y-1 >= 0:
					if level[x][y-1].astype(int) == 2:
						wallCount = wallCount + 1
				else:
					wallCount = wallCount + 1

				if wallCount > 2:
					return False

	return True
#----------------------------------------------------------------------

# Creates a sudo random map and checks against constraints
# keeps trying until a map is created
#
# @args: small: the minimum map dimension
#        large: the maximum map dimension
#
# @return: A good playable map
def getGoodRoom(small, large):
	found = False
	while not found:
		level = emptyRoom(small,large)
		found = True

		found = onlyOneContFloor(level)

		if found:
			found = noOpenFloorToBig(level)

		
		if found:
			found = noTrappedFloors(level)


		#if not found:
		#	print "bad"
	return level
#----------------------------------------------------------------------

# Map generator -------------------------------------------------------

# Randomly puts a player on the map
# only place on floor space
#
# @args: level: current map
#
# @return: map with player added
def addPlayer(level):
	keepGoing = True

	while keepGoing:
		x = random.randint(0,level.shape[0]-1)
		y = random.randint(0,level.shape[1]-1)

		if level[x][y] == 1:
			keepGoing = False
			level[x][y] = 3

	return level
#----------------------------------------------------------------------

# Randomly put goals on the map
# only put on floor space
# has an error timeout to stop if its impossible to add goals 
# this timeout should never happen, but just in case
#
# @args: level: current map, numberOfGoals: the int number of goals to place
#
# @return level wit goals added
def addGoal(level, numberOfGoals):

	keepGoing = True
	count = 0

	while keepGoing:
		x = random.randint(0,level.shape[0]-1)
		y = random.randint(0,level.shape[1]-1)

		if level[x][y] == 1:
			level[x][y] = 4
			numberOfGoals = numberOfGoals - 1

		if numberOfGoals <= 0:
			keepGoing = False

		count = count + 1
		if count > 10000:
			break

	return level
#----------------------------------------------------------------------

# Randomly put boxes on the map
# only put on floor space
# has an error timeout to stop if its impossible to add goals 
# this timeout should never happen, but just in case
#
# @args: level: current map, numberOfBoxes: the int number of boxes to place
#
# @return level wit boxes added
def addBox(level, numberOfBoxes):

	keepGoing = True
	count = 0

	while keepGoing:
		x = random.randint(0,level.shape[0]-2)
		y = random.randint(0,level.shape[1]-2)

		if level[x][y] == 1:
			level[x][y] = 5
			numberOfBoxes = numberOfBoxes - 1

		if numberOfBoxes <= 0:
			keepGoing = False

		count = count + 1
		if count > 10000:
			break

	return level
#----------------------------------------------------------------------

# Prints to console
#
# @args: level: current map
def printLevel(level):
	for x in range(0,level.shape[1]+2):
		sys.stdout.write("#")
	print ""

	for row in level:
		sys.stdout.write("#")
		for i in row:
			if i == 2:
				sys.stdout.write("#")
			elif i == 1:
				sys.stdout.write(" ")
			elif i == 3:
				sys.stdout.write("@")
			elif i == 4:
				sys.stdout.write(".")
			elif i == 5:
				sys.stdout.write("$")
		print "#"

	for x in range(0,level.shape[1]+2):
		sys.stdout.write("#")
#----------------------------------------------------------------------

# Converts map to a string representation
#
# @args: level: current map
#
# @return: string of level
def createString(level):
	string = ""
	for x in range(0,level.shape[1]+2):
		string = string + "#"
	string = string + "\n"

	for row in level:
		string = string + "#" 
		for i in row:
			if i == 2:
				string = string + "#"
			elif i == 1:
				string = string + " "
			elif i == 3:
				string = string + "@"
			elif i == 4:
				string = string + "."
			elif i == 5:
				string = string + "$"
		string = string + "#\n"

	for x in range(0,level.shape[1]+2):
		string = string + "#"

	string = string + "\n"

	return string
#----------------------------------------------------------------------

# Count the number of floor spaces in level
# Used to determine max number of objects that can be placed
#
# @args: level: current map
#
# @return: int of number of floors tiles
def countFloorSpaces(level):
	floors = 0
	for ch in level:
		if ch == ' ':
			floors = floors + 1
	
	return floors
#----------------------------------------------------------------------

# Creates near complete level set for a map
# creates a valid map of min-max size
# populates this map with objects starting with 2
# and working its way up to max objects
# checks each level to make sure it is solvable
# if it is adds it to queue with solution
# else throws away (keeps list of created level to avoid duplicates)
# 
# has several outs to keep it running, don't waste resources on levels
# that can't be solved with given resources (some results better than none)
def solvablelevel():
	levelSet = Queue.Queue()
	good = True
	strLevel = ""
	count = 0
	obj = 3  #changed from 2 to 3, 3 make much better levels
	visited = set([strLevel])
	v_count = 0
	solution_errors = 0
	
	level = getGoodRoom(6, 15)
	
	maxObj = countFloorSpaces(createString(level))
	maxObj = maxObj / 2
	print maxObj

	print level
	
	while good:
		good = False
		tempLevel = level.copy()
		tempLevel = addPlayer(tempLevel)
		tempLevel = addGoal(tempLevel, obj)
		tempLevel = addBox(tempLevel, obj)

		strLevel =  createString(tempLevel)
		
		if strLevel not in visited:
			visited.add(strLevel)
			
			print "solving"
			sokoban_solver.init(strLevel)
			#strcatch = sokoban_solver.solve()
			
			outgoing = ThreadQueue()
			
			p = Process(target=sokoban_solver.solve, args=(5,outgoing))
			p.start()
			p.join(60)		
			if p.is_alive():
				solution_errors = solution_errors + 1
				print "stuck... let's kill it..."
				# Terminate
				p.terminate()
				p.join()
				strcatch = "No Solution"
			else:
				strcatch = outgoing.get(block=False, timeout=None)
				outgoing.close()
	
			#print strcatch
			if strcatch == "No Solution" or strcatch == "None":
				count = count + 1
				print "no solution"
				good = True
				sokoban_solver.data = []
				sokoban_solver.nrows = 0
				sokoban_solver.px = sokoban_solver.py = 0
				sokoban_solver.sdata = ""
				sokoban_solver.ddata = ""
			else:
				count = 0
				print "add to set"
				levelSet.put((strLevel,strcatch))
				good = True
				
			if count > 1000:
				count = 0
				obj = obj + 1
			if obj > maxObj:
				break
		else:
			print "duplicated"
			good = True
			v_count = 1 + v_count
		
		if v_count > 1000:
			v_count = 0
			obj = obj + 1
			print obj

		if solution_errors > 100:
			print "break early"
			break;
	#print strLevel
	
	return levelSet
#----------------------------------------------------------------------

# Get Push, Move, and Boxline rankings for each level
#
# @args: solution: string of best solutions moves
#
# @return: tuple of rankings
def traditionalRating(solution):
	moves = len(solution)
	push = 0
	boxLines = 0
	
	# count pushes
	for ch in solution:
		if (ch == 'L') or (ch =='R') or (ch == 'U') or (ch == 'D'):
			push = push + 1
	
	# count boxlines
	i = 0
	while i < moves:
		ch = solution[i]
		if (ch == 'L') or (ch =='R') or (ch == 'U') or (ch == 'D'):
			temp = solution[i]
			boxLines = boxLines + 1
			while solution[i] == temp and i<moves-1:
				i = i+1
		i = i+1
				
			
	
	return (moves, push, boxLines)
#----------------------------------------------------------------------

# Main ----------------------------------------------------------------
catch = solvablelevel()

#writes level set to map
myFile = open("levelSet01.txt", 'w')

# find min and max of each rank
count = 1
maxPush = 0
maxMoves = 0
maxBoxLines = 0
minPush = 1000000000
minMoves = 1000000000
minBoxLines = 1000000000
print "level set"
while not catch.empty():
	temp = catch.get()
	myFile.write("Level " + str(count) +"\n")
	myFile.write(temp[0])
	myFile.write(temp[1]+"\n")
	t = traditionalRating(temp[1])
	if t[0] < minMoves:
		minMoves = t[0]
	if t[0] > maxMoves:
		maxMoves = t[0]
	if t[1] < minPush:
		minPush = t[1]
	if t[1] > maxPush:
		maxPush = t[1]
	if t[2] < minBoxLines:
		minBoxLines = t[2]
	if t[2] > maxBoxLines:
		maxBoxLines = t[2]
	myFile.write("Moves: " + str(t[0])+"\n")
	myFile.write("Pushes: " + str(t[1])+"\n")
	myFile.write("BoxLines: " + str(t[2])+"\n")
	count = count + 1
	myFile.write("\n")
		
myFile.write("Max Moves: " + str(maxMoves)+"\n")
myFile.write("Min Moves: " + str(minMoves)+"\n")
myFile.write("Max Pushes: " + str(maxPush)+"\n")
myFile.write("Min Pushes: " + str(minPush)+"\n")
myFile.write("Max BoxLines: " + str(maxBoxLines)+"\n")
myFile.write("Min BoxLines: " + str(minBoxLines)+"\n")

myFile.close()
print "done" + str(count)
# End Main -------------------------------------------------------------