-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2015 at 12:09 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sokobanlevel`
--

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `id` int(11) NOT NULL,
  `played` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `games` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `played`, `created`, `games`) VALUES
(1, 178, 12621, 45);

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE IF NOT EXISTS `levels` (
  `level_ID` int(11) NOT NULL AUTO_INCREMENT,
  `level_Str` text,
  `level_Solution` text,
  `moves_Rank` int(11) NOT NULL,
  `push_Rank` int(11) NOT NULL,
  `boxLine_Rank` int(11) NOT NULL,
  `user_Rank` int(11) NOT NULL,
  `my_Rank` int(11) NOT NULL,
  `user_submitted` int(11) NOT NULL,
  PRIMARY KEY (`level_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`level_ID`, `level_Str`, `level_Solution`, `moves_Rank`, `push_Rank`, `boxLine_Rank`, `user_Rank`, `my_Rank`, `user_submitted`) VALUES
(2, '########!##  ####!#@ $  .#!# .#   #!#####  #!#####$ #!####   #!##  $.##!##   ###!########', 'rRRdrruLLrrddddldLdlluRRurUUUluRlLulD\n', 37, 13, 8, 4, 15, 3),
(3, '########!####   #!## $   #!## #  .#!#  .####!# #$  ##!#@$ # ##!# .   ##!########!', 'uuruuRRRurDllllddrDDuullddRdRUUrrddLL', 1, 1, 1, 4, 6, 4),
(4, '########!#   #  #!#$# #  #!#   #$ #!#. #  ##!#    $.#!##. #@ #!########!', 'ruLLurUruulDDDldRlLdlUluurruullDDrDD\n', 36, 13, 8, 4, 26, 4),
(5, '########!####  .#!#### # #!#### $ #!#   $..#!#@$ ####!#   ####!########!########!', 'urrRuuurrdddLLuRdrUUddllLddlluuRRRRllddlUluRRR\n', 46, 15, 8, 5, 31, 3),
(6, '##########!#   ###  #!# #   #  #!#@  # #  #!## #  . ##!## # #   #!## # $   #!#  ### # #!# $ ## # #!##. ## # #!####     #!####     #!##########!', 'rddddDuuuuururrddlddRRurrdLrddddlluuuUU\n', 10, 10, 10, 2, 5, 2),
(7, '########!#####  #!#### $ #!####.  #!# @$.###!# #. ###!#  $ ###!#   ####!########!', 'lddrdrUruuLDrdLuururruulDrdLulDDlddrUUddldlluRdrUlluuRR\n', 56, 14, 9, 3, 10, 6),
(8, '###########!#.  ###   #!#$#     #@#!#   ###   #!#  ##  ####!# $    ####!#. #  #####!###########!', 'ulldlllldldlddrUluurruullDDDDrUluuurrddLdlUU\n', 44, 9, 5, 5, 0, 4),
(9, '###########!#   ###   #!# #     # #!#   ###   #!# $##. ####!#.$@   ####!#  #  #####!###########!', 'LdlUUUrDldRuuruullDDDrdRRdrU\n', 28, 12, 7, 4, 0, 5),
(10, '########!####.. #!####   #!##### ##!##@ $ ##!## $  ##!##  ####!########!', 'rRdrUUUruLdddlllddrUluRRdrUUU\n', 29, 11, 6, 2, 0, 5),
(11, '########!####.. #!####   #!##### ##!##@ $ ##!## $  ##!##  ####!########!', 'rRdrUUUruLdddlllddrUluRRdrUUU\n', 29, 11, 6, 3, 0, 6),
(12, '########!####.. #!####   #!##### ##!##@ $ ##!## $  ##!##  ####!########!', 'rRdrUUUruLdddlllddrUluRRdrUUU\n', 29, 11, 6, 2, 0, 3),
(13, '########!########!########!########!####  .#!####$. #!# $    #!#   # @#!########!', 'ullldlluRRRRdrUUlulDrdLLdlluRRRurrddlU\n', 38, 13, 6, 3, 0, 7),
(14, '########!########!########!########!#### @.#!####$ .#!#   $  #!#   #  #!########!', 'ddLLdlluRRRRdrUUlulDrdLLdlluRRRRdrU\n', 35, 16, 7, 3, 0, 4),
(15, '########!########!########!########!####@  #!####$ .#!#   $  #!#   #. #!########!', 'rddLLdlluRRRRuulDrddrUlLLdlluRRRurD\n', 35, 14, 7, 4, 0, 7),
(16, '########!#   ..##!# #    #!# $  $@#!#   ####!#   ####!##  ####!########!', 'LLuullldddrrUUddlluuurRRlllddRdrUUdlluurR\n', 41, 10, 6, 3, 0, 3),
(17, '########!#     ##!# #  $ #!#.    @#!#   ####!#. $####!##  ####!########!', 'uLulDrdLLLruulldDDrddrUUUruulDrdLL\n', 34, 13, 7, 4, 0, 4),
(18, '########!#     ##!# #    #!# .$   #!#   ####!#. $####!## @####!########!', 'UluuRurrdLLLddrUUruulDrdLuulldDD\n', 32, 11, 7, 3, 0, 3),
(19, '############!#   ####  ##!#@# ####   #!#  .####   #!#   #### $##!#.  $     ##!#  #      ##!#####  #####!############!', 'drrddRdrruLLLullddrUrrrrrrrUUruLulDDDrdLLLLLLLUluR\n', 50, 20, 8, 4, 0, 4),
(20, '#########!#.$ .   #!# #.  $ #!#   #####!##  #####!# $ #####!#@ ######!#########!', 'rUruuuurrrrdLLullddlluuRRddddlUruuullddRluurrdDuuRRdLulllddrrUruLLrrrdrruLLdlldlddrUU\n', 85, 18, 12, 7, 0, 4),
(21, '#########!#.. .   #!# #   $ #!#   #####!##$$#####!#  @#####!#  ######!#########!', 'UUUrurrrdLLulLddlluuRRddddlUruuullddRluurrdDuuRRdLulllddrrUruLLrddlddrUUUruLrrdrruLL\n', 84, 23, 14, 6, 0, 5),
(22, '########!####   #!##   $ #!## #.  #!#. #####!# ######!#  #####!# $ ####!#   ####!## #####!#  # @##!#     ##!########!', 'dllluuuruLuluuruurrurrdLulDllddlddrddlUUU\n', 41, 6, 4, 3, 0, 3),
(23, '########!####   #!## $   #!## #  .#!#  #####!# ######!#  #####!#   ####!#   ####!## #####!#@$#  ##!#    .##!########!', 'drUUUluurDDDDldRRRlluuuuuluuruuRRRurD\n', 37, 14, 5, 3, 0, 3),
(24, '########!####   #!## $   #!## #.  #!#  #####!#.######!#  #####!#   ####!#   ####!##$#####!#  #@ ##!#     ##!########!', 'dlluUUruLuluuruuRRurrdLulDllddlddrddlUU\n', 39, 9, 6, 2, 0, 4),
(25, '########!#.  $@##!#$  $ ##!# ######!#. #####!#. #####!########!########!', 'dLLullDDDuuurrdLulDDurrrruLLL\n', 29, 11, 5, 3, 0, 2),
(26, '########!###.@###!###.  ##!### $ ##!# $ ####!# # ####!#   ####!########!', 'dlddddlluuRlddrruUUUrrdLulDDllddrrUUU\n', 37, 10, 5, 3, 0, 5),
(27, '########!###. ###!###. @##!### $ ##!#  $####!# # ####!#   ####!########!', 'lldDllddrrUUUUrrdLulDDllddrrUUU\n', 31, 11, 5, 4, 0, 7),
(28, '########!#.  ####!#   ####!## #####!## ##  #!#@$$   #!#  #.  #!########!', 'drUUUUruLddddRRdrruulDrdL\n', 25, 9, 5, 2, 0, 3),
(29, '########!# . ####!#   ####!##$#####!## ##  #!#.$    #!#@ #   #!########!', 'uRRRdrruLLLLuUU\n', 15, 9, 3, 2, 0, 4),
(30, '########!#  .####!#   ####!##$#####!## ##  #!#@$    #!#  #.  #!########!', 'RuUUluRddddRRdrruulDrdL\n', 23, 8, 6, 3, 0, 2),
(31, '##########!# .#     #!#     #@ #!## ##### #!##$ ###  #!##  ###$ #!##  ###. #!##########!', 'rddlDuruuullldllldDrddlUUUU\n', 27, 6, 3, 1, 0, 4),
(32, '##########!# .#.  $ #!#     #  #!## ##### #!##$ ###  #!##  ### @#!##  ###  #!##########!', 'uuuuLLLdllldDrddlUUUU\n', 21, 8, 3, 1, 0, 4),
(33, '########!## .####!## .####!### ####!#@$$####!# $.####!#   ####!########!', 'ddrruUUUddddlluuRlddrruUUdlldR\n', 30, 7, 4, 4, 0, 8),
(34, '########!##@.####!## .####!###$####!#   ####!# $ ####!#   ####!########!', 'rdDDllddrrUUUUddlldRdrUUU\n', 25, 10, 4, 1, 0, 2),
(35, '########!##@ ####!## .####!### ####!#. $####!# $ ####!#   ####!########!', 'rddDllddrUdrUUUdL\n', 17, 6, 4, 3, 0, 5),
(36, '########!#    ###!# #$ ###!#  @ ###!####.$ #!####.# #!####   #!########!', 'ruulDrddddrruuLrddlluUULuurDDDuuulllddRRurD\n', 43, 11, 6, 4, 0, 6),
(37, '########!#   .###!#@# .###!#  $$###!####   #!#### # #!####   #!########!', 'urrrdDDrrddllUUUUlullddrRurDDrrddllUUU\n', 38, 12, 5, 4, 0, 5),
(38, '########!#    ###!# #$ ###!#    ###!####.$ #!####.# #!#### @ #!########!', 'ruuLrddlluUUllluurrrDDDuuulDullddrRurD\n', 38, 9, 6, 4, 0, 5),
(39, '########!#..   ##!# # $$ #!#  .#  #!### # ##!###$   #!###   @#!########!', 'lllUUUlluurrrrDDDuuullllddrrURuLLrddddrdrruLLdlUUUUdddrruuruLLuLD\n', 65, 20, 8, 5, 0, 2),
(40, '########!#   ####!# #$####!#  .####!## #  ##!## # $##!## #   #!#  ## @#!# ###  #!#  ##  #!#    . #!########!', 'dddlllluluuruuuuluurrDullddrddddlddrdrrruuuuluurDDDDD\n', 53, 6, 2, 2, 0, 4),
(41, '########!#   ####!# # ####!#   ####!## #  ##!## #  ##!## # $ #!# @##  #!# ###$ #!#  ##  #!#.  .  #!########!', 'lddrdrrruruulDDrdLLLLrrruuuUluurDDDDDrdL\n', 40, 13, 5, 2, 0, 2),
(42, '########!#   ####!# # ####!#@  ####!## #  ##!## # $##!## #.  #!#  ##  #!# ###$ #!#  ##  #!#   .  #!########!', 'rddddlddrdrrruruulDuuluurDDrddlUruLddDrdL\n', 41, 7, 6, 2, 0, 1),
(43, '########!########!##   .##!## ## ##!# $##  #!#      #!#  ##  #!# ######!#  ##  #!#  ##$@#!##  .  #!########!', 'dlllluuluuurrrruuullldDldRRRlluuurrrddrddlUUUddllldlddrddrrrruulDrdL\n', 68, 9, 5, 4, 0, 3),
(44, '########!########!##    ##!## ##$##!#  ##  #!#@  $  #!#  ##  #!# ######!#. ##  #!#. ##  #!##     #!########!', 'uruurrrDDrddlULLLulDDDDuuuuruurrrdDrdLLLLulDDD\n', 46, 18, 7, 3, 0, 2),
(45, '########!########!##    ##!## ##$##!#. ##  #!#  $   #!#  ##  #!# ######!#  ##  #!#. ## @#!##     #!########!', 'dlllluuluuuuruurrrDDrddlUlLLulDDuuruurrrdDrdLLLLdlUdDD\n', 54, 15, 8, 5, 0, 6);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
