'''
Created on Nov 16, 2015

@author: Tyson
'''
import mysql.connector
from mysql.connector import errorcode
import sokoban_explore_from_solution

def read_apply_write():
    try:
        cnx = mysql.connector.connect(user='root', password='root',
                                  host='localhost',
                                  database='sokobanlevel',
                                  port='8889')
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    
    print "good to go"
    
    cursor = cnx.cursor()
    
    get_level = ("SELECT `level_ID`, `level_Str`, `level_Solution`, `my_Rank` FROM `Levels`")

    cursor.execute(get_level)
    
    rank_arr = []
    for( level_ID, level_Str, level_Solution, my_Rank) in cursor:
        if my_Rank == 0:
            #print level_Str + " **** " + level_Solution
            rank = sokoban_explore_from_solution.RankLevel()
            rank.create_winning_set(level_Str, level_Solution)
            rank.init_grid(level_Str)
            dbRank = rank.number_of_badMoves()
            rank_arr.append((level_ID, dbRank))
            print str(level_ID) + ": " + str(dbRank)
            
    for temp in rank_arr:
        update_rank = ("UPDATE `levels` SET `my_Rank`=%s WHERE `level_ID`=%s")
        pmet = (str(temp[1]), str(temp[0]))
        cursor.execute(update_rank, pmet)
        cnx.commit()
    
    
    cursor.close()
    cnx.close()
    
read_apply_write()
    