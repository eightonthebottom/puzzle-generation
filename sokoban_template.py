import numpy

###############################################
#     A series of 17 templates                #
#                                             #
#  templates used to create sudo-random maps  #
###############################################

# Templates come from paper written by Joshua Taylor and Ian Parberry
# Procedural Generation of Sokoban Level Laboratory for Recreational Computing (February 2011) by Joshua Taylor, Ian Parberry

template = []

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 1
temp[1][2] = 1
temp[1][3] = 1
temp[1][4] = 0

temp[2][0] = 0
temp[2][1] = 1
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 0

temp[3][0] = 0
temp[3][1] = 1
temp[3][2] = 1
temp[3][3] = 1
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 1
temp[1][3] = 1
temp[1][4] = 0

temp[2][0] = 0
temp[2][1] = 1
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 0

temp[3][0] = 0
temp[3][1] = 1
temp[3][2] = 1
temp[3][3] = 1
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 1
temp[0][4] = 1

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 2
temp[1][3] = 1
temp[1][4] = 1

temp[2][0] = 0
temp[2][1] = 1
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 0

temp[3][0] = 0
temp[3][1] = 1
temp[3][2] = 1
temp[3][3] = 1
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 2
temp[1][3] = 2
temp[1][4] = 0

temp[2][0] = 0
temp[2][1] = 1
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 0

temp[3][0] = 0
temp[3][1] = 1
temp[3][2] = 1
temp[3][3] = 1
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 2
temp[1][3] = 2
temp[1][4] = 0

temp[2][0] = 0
temp[2][1] = 2
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 0

temp[3][0] = 0
temp[3][1] = 2
temp[3][2] = 1
temp[3][3] = 1
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 1
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 1
temp[1][3] = 1
temp[1][4] = 0

temp[2][0] = 1
temp[2][1] = 1
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 0

temp[3][0] = 0
temp[3][1] = 1
temp[3][2] = 1
temp[3][3] = 2
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 1
temp[1][3] = 1
temp[1][4] = 0

temp[2][0] = 1
temp[2][1] = 1
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 0

temp[3][0] = 0
temp[3][1] = 2
temp[3][2] = 1
temp[3][3] = 1
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 1
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 1
temp[1][3] = 1
temp[1][4] = 0

temp[2][0] = 1
temp[2][1] = 1
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 0

temp[3][0] = 0
temp[3][1] = 2
temp[3][2] = 1
temp[3][3] = 2
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 1
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 1
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 1
temp[1][3] = 2
temp[1][4] = 0

temp[2][0] = 1
temp[2][1] = 1
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 1

temp[3][0] = 0
temp[3][1] = 2
temp[3][2] = 1
temp[3][3] = 2
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 1
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 1
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 1
temp[1][3] = 2
temp[1][4] = 0

temp[2][0] = 0
temp[2][1] = 2
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 1

temp[3][0] = 0
temp[3][1] = 2
temp[3][2] = 2
temp[3][3] = 2
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 2
temp[1][3] = 2
temp[1][4] = 0

temp[2][0] = 1
temp[2][1] = 1
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 1

temp[3][0] = 0
temp[3][1] = 2
temp[3][2] = 2
temp[3][3] = 2
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 1
temp[1][2] = 1
temp[1][3] = 1
temp[1][4] = 1

temp[2][0] = 0
temp[2][1] = 1
temp[2][2] = 2
temp[2][3] = 1
temp[2][4] = 1

temp[3][0] = 0
temp[3][1] = 1
temp[3][2] = 1
temp[3][3] = 1
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 2
temp[1][3] = 2
temp[1][4] = 0

temp[2][0] = 0
temp[2][1] = 2
temp[2][2] = 2
temp[2][3] = 2
temp[2][4] = 0

temp[3][0] = 0
temp[3][1] = 2
temp[3][2] = 2
temp[3][3] = 2
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 0
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 2
temp[1][3] = 2
temp[1][4] = 0

temp[2][0] = 0
temp[2][1] = 2
temp[2][2] = 1
temp[2][3] = 1
temp[2][4] = 0

temp[3][0] = 1
temp[3][1] = 1
temp[3][2] = 1
temp[3][3] = 1
temp[3][4] = 0

temp[4][0] = 1
temp[4][1] = 1
temp[4][2] = 0
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 1
temp[0][2] = 0
temp[0][3] = 1
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 1
temp[1][2] = 1
temp[1][3] = 1
temp[1][4] = 0

temp[2][0] = 0
temp[2][1] = 2
temp[2][2] = 1
temp[2][3] = 2
temp[2][4] = 0

temp[3][0] = 0
temp[3][1] = 1
temp[3][2] = 1
temp[3][3] = 1
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 1
temp[4][2] = 0
temp[4][3] = 1
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 2
temp[1][3] = 2
temp[1][4] = 0

temp[2][0] = 0
temp[2][1] = 2
temp[2][2] = 2
temp[2][3] = 2
temp[2][4] = 0

temp[3][0] = 0
temp[3][1] = 1
temp[3][2] = 1
temp[3][3] = 1
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 1
temp[4][2] = 1
temp[4][3] = 1
temp[4][4] = 0

template.append(temp)

temp = numpy.ndarray((5,5))

temp[0][0] = 0
temp[0][1] = 0
temp[0][2] = 0
temp[0][3] = 0
temp[0][4] = 0

temp[1][0] = 0
temp[1][1] = 2
temp[1][2] = 2
temp[1][3] = 2
temp[1][4] = 0

temp[2][0] = 1
temp[2][1] = 1
temp[2][2] = 2
temp[2][3] = 1
temp[2][4] = 1

temp[3][0] = 0
temp[3][1] = 1
temp[3][2] = 1
temp[3][3] = 1
temp[3][4] = 0

temp[4][0] = 0
temp[4][1] = 1
temp[4][2] = 1
temp[4][3] = 0
temp[4][4] = 0

template.append(temp)


template2 = []

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 0
temp2[0][3] = 0
temp2[0][4] = 0
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 1
temp2[1][2] = 1
temp2[1][3] = 1
temp2[1][4] = 1
temp2[1][5] = 1
temp2[1][6] = 0

temp2[2][0] = 0
temp2[2][1] = 1
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 0

temp2[3][0] = 0
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 0

temp2[4][0] = 0
temp2[4][1] = 1
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 1
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 1
temp2[5][5] = 1
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 0
temp2[6][3] = 0
temp2[6][4] = 0
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 0
temp2[0][3] = 0
temp2[0][4] = 0
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 1
temp2[1][4] = 1
temp2[1][5] = 1
temp2[1][6] = 0

temp2[2][0] = 0
temp2[2][1] = 2
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 0


temp2[3][0] = 0
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 0

temp2[4][0] = 0
temp2[4][1] = 1
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 1
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 1
temp2[5][5] = 1
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 0
temp2[6][3] = 0
temp2[6][4] = 0
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 0
temp2[0][3] = 0
temp2[0][4] = 1
temp2[0][5] = 1
temp2[0][6] = 1

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 2
temp2[1][4] = 1
temp2[1][5] = 1
temp2[1][6] = 1

temp2[2][0] = 0
temp2[2][1] = 1
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 1

temp2[3][0] = 0
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 0

temp2[4][0] = 0
temp2[4][1] = 1
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 1
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 1
temp2[5][5] = 1
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 0
temp2[6][3] = 0
temp2[6][4] = 0
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 0
temp2[0][3] = 0
temp2[0][4] = 0
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 2
temp2[1][4] = 2
temp2[1][5] = 2
temp2[1][6] = 0

temp2[2][0] = 0
temp2[2][1] = 1
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 0

temp2[3][0] = 0
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 0

temp2[4][0] = 0
temp2[4][1] = 1
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 1
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 1
temp2[5][5] = 1
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 0
temp2[6][3] = 0
temp2[6][4] = 0
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 0
temp2[0][3] = 0
temp2[0][4] = 0
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 2
temp2[1][4] = 2
temp2[1][5] = 2
temp2[1][6] = 0

temp2[2][0] = 0
temp2[2][1] = 2
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 0

temp2[3][0] = 0
temp2[3][1] = 2
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 0

temp2[4][0] = 0
temp2[4][1] = 2
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 2
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 1
temp2[5][5] = 1
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 0
temp2[6][3] = 0
temp2[6][4] = 0
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 0
temp2[0][3] = 1
temp2[0][4] = 1
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 1
temp2[1][4] = 1
temp2[1][5] = 1
temp2[1][6] = 0

temp2[2][0] = 0
temp2[2][1] = 2
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 0

temp2[3][0] = 1
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 0

temp2[4][0] = 1
temp2[4][1] = 1
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 2
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 1
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 2
temp2[5][5] = 2
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 0
temp2[6][3] = 0
temp2[6][4] = 0
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 0
temp2[0][3] = 0
temp2[0][4] = 0
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 1
temp2[1][4] = 1
temp2[1][5] = 1
temp2[1][6] = 0

temp2[2][0] = 1
temp2[2][1] = 1
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 0

temp2[3][0] = 1
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 0

temp2[4][0] = 1
temp2[4][1] = 2
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 2
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 1
temp2[5][5] = 1
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 0 
temp2[6][3] = 0
temp2[6][4] = 0
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)


temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 1
temp2[0][3] = 1
temp2[0][4] = 1
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 1
temp2[1][3] = 1
temp2[1][4] = 1
temp2[1][5] = 1
temp2[1][6] = 0

temp2[2][0] = 1
temp2[2][1] = 2
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 0

temp2[3][0] = 1
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 0

temp2[4][0] = 1
temp2[4][1] = 1
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 2
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 2
temp2[5][2] = 2
temp2[5][3] = 1
temp2[5][4] = 1
temp2[5][5] = 2
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 1
temp2[6][3] = 1
temp2[6][4] = 1
temp2[6][5] = 0
temp2[6][6] = 0
template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 1
temp2[0][3] = 1
temp2[0][4] = 1
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 1
temp2[1][4] = 1
temp2[1][5] = 2
temp2[1][6] = 0

temp2[2][0] = 1
temp2[2][1] = 1
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 2
temp2[2][6] = 1

temp2[3][0] = 1
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 1

temp2[4][0] = 1
temp2[4][1] = 2
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 1

temp2[5][0] = 0
temp2[5][1] = 2
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 2
temp2[5][5] = 2
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 1
temp2[6][3] = 1
temp2[6][4] = 1
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 1
temp2[0][3] = 1
temp2[0][4] = 1
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 1
temp2[1][3] = 1
temp2[1][4] = 2
temp2[1][5] = 2
temp2[1][6] = 0

temp2[2][0] = 0
temp2[2][1] = 2
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 1

temp2[3][0] = 0
temp2[3][1] = 2
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 1

temp2[4][0] = 0
temp2[4][1] = 2
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 1

temp2[5][0] = 0
temp2[5][1] = 2
temp2[5][2] = 2
temp2[5][3] = 2
temp2[5][4] = 2
temp2[5][5] = 2
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 0
temp2[6][3] = 0
temp2[6][4] = 0
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 0
temp2[0][3] = 0
temp2[0][4] = 0
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 2
temp2[1][4] = 2
temp2[1][5] = 2
temp2[1][6] = 0

temp2[2][0] = 1
temp2[2][1] = 1
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 1

temp2[3][0] = 1
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 1

temp2[4][0] = 1
temp2[4][1] = 1
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 1

temp2[5][0] = 0
temp2[5][1] = 2
temp2[5][2] = 2
temp2[5][3] = 2
temp2[5][4] = 2
temp2[5][5] = 2
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 0
temp2[6][3] = 0
temp2[6][4] = 0
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 0
temp2[0][3] = 0
temp2[0][4] = 0
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 2
temp2[1][4] = 2
temp2[1][5] = 2
temp2[1][6] = 0

temp2[2][0] = 1
temp2[2][1] = 1
temp2[2][2] = 1
temp2[2][3] = 2
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 1

temp2[3][0] = 1
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 2
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 1

temp2[4][0] = 1
temp2[4][1] = 1
temp2[4][2] = 1
temp2[4][3] = 2
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 1

temp2[5][0] = 0
temp2[5][1] = 1
temp2[5][2] = 1
temp2[5][3] = 1 
temp2[5][4] = 1
temp2[5][5] = 1
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 1
temp2[6][2] = 1
temp2[6][3] = 1
temp2[6][4] = 1
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 0
temp2[0][3] = 0
temp2[0][4] = 0
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 1
temp2[1][2] = 1
temp2[1][3] = 1
temp2[1][4] = 1
temp2[1][5] = 1
temp2[1][6] = 1

temp2[2][0] = 0
temp2[2][1] = 1
temp2[2][2] = 1
temp2[2][3] = 2
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 1

temp2[3][0] = 0
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 2
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 1

temp2[4][0] = 0
temp2[4][1] = 1
temp2[4][2] = 1
temp2[4][3] = 2
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 1
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 1
temp2[5][5] = 1
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 0
temp2[6][2] = 0
temp2[6][3] = 0
temp2[6][4] = 0
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 2
temp2[0][2] = 2
temp2[0][3] = 2
temp2[0][4] = 2
temp2[0][5] = 2
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 2
temp2[1][4] = 2
temp2[1][5] = 2
temp2[1][6] = 0

temp2[2][0] = 0
temp2[2][1] = 2
temp2[2][2] = 2
temp2[2][3] = 2
temp2[2][4] = 2
temp2[2][5] = 2
temp2[2][6] = 0

temp2[3][0] = 0
temp2[3][1] = 2
temp2[3][2] = 2
temp2[3][3] = 2
temp2[3][4] = 2
temp2[3][5] = 2
temp2[3][6] = 0

temp2[4][0] = 0
temp2[4][1] = 2
temp2[4][2] = 2
temp2[4][3] = 2
temp2[4][4] = 2
temp2[4][5] = 2
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 2
temp2[5][2] = 2
temp2[5][3] = 2
temp2[5][4] = 2
temp2[5][5] = 2
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 2
temp2[6][2] = 2
temp2[6][3] = 2
temp2[6][4] = 2
temp2[6][5] = 2
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 0
temp2[0][2] = 0
temp2[0][3] = 0
temp2[0][4] = 0
temp2[0][5] = 0
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 2
temp2[1][4] = 2
temp2[1][5] = 2
temp2[1][6] = 0

temp2[2][0] = 0
temp2[2][1] = 2
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 1
temp2[2][6] = 0

temp2[3][0] = 0
temp2[3][1] = 2
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 0

temp2[4][0] = 1
temp2[4][1] = 1
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 0

temp2[5][0] = 1
temp2[5][1] = 1
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 1
temp2[5][5] = 1
temp2[5][6] = 0

temp2[6][0] = 1
temp2[6][1] = 1
temp2[6][2] = 1
temp2[6][3] = 0
temp2[6][4] = 0
temp2[6][5] = 0
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 1
temp2[0][2] = 1
temp2[0][3] = 0
temp2[0][4] = 1
temp2[0][5] = 1
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 1
temp2[1][2] = 1
temp2[1][3] = 1
temp2[1][4] = 1
temp2[1][5] = 1
temp2[1][6] = 0

temp2[2][0] = 0
temp2[2][1] = 2
temp2[2][2] = 1
temp2[2][3] = 1
temp2[2][4] = 1
temp2[2][5] = 2
temp2[2][6] = 0

temp2[3][0] = 0
temp2[3][1] = 2
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 2
temp2[3][6] = 0

temp2[4][0] = 0
temp2[4][1] = 2
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 2
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 1
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 1
temp2[5][5] = 1
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 1
temp2[6][2] = 1
temp2[6][3] = 0
temp2[6][4] = 1
temp2[6][5] = 1
temp2[6][6] = 0

template2.append(temp2)

temp2 = numpy.ndarray((7,7))

temp2[0][0] = 0
temp2[0][1] = 2
temp2[0][2] = 2
temp2[0][3] = 2
temp2[0][4] = 2
temp2[0][5] = 2
temp2[0][6] = 0

temp2[1][0] = 0
temp2[1][1] = 2
temp2[1][2] = 2
temp2[1][3] = 2
temp2[1][4] = 2
temp2[1][5] = 2
temp2[1][6] = 0

temp2[2][0] = 0
temp2[2][1] = 2
temp2[2][2] = 2
temp2[2][3] = 2
temp2[2][4] = 2
temp2[2][5] = 2
temp2[2][6] = 0

temp2[3][0] = 0
temp2[3][1] = 1
temp2[3][2] = 1
temp2[3][3] = 1
temp2[3][4] = 1
temp2[3][5] = 1
temp2[3][6] = 0

temp2[4][0] = 0
temp2[4][1] = 1
temp2[4][2] = 1
temp2[4][3] = 1
temp2[4][4] = 1
temp2[4][5] = 1
temp2[4][6] = 0

temp2[5][0] = 0
temp2[5][1] = 1
temp2[5][2] = 1
temp2[5][3] = 1
temp2[5][4] = 1
temp2[5][5] = 1
temp2[5][6] = 0

temp2[6][0] = 0
temp2[6][1] = 1
temp2[6][2] = 1
temp2[6][3] = 1
temp2[6][4] = 1
temp2[6][5] = 1
temp2[6][6] = 0

template2.append(temp2)